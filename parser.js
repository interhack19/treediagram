function clone(obj) {
	if (obj === null || typeof obj !== 'object') {
		return obj;
	}
	var temp = obj.constructor(); // give temp the original obj's constructor
	for (var key in obj) {
		temp[key] = clone(obj[key]);
	}
	return temp;
}

function parse(data) {
	data2 = {
		"name": data[0].title,
		"state": data[0].state,
		"description": data[0].description,
		"id": data[0].id
	}
	function addChildren(datarec, id) {
		datarec.children = []
		for (var d in data) {
			if (data[d].dependsOn[0] == id) {
				datarec.children.push({"name": data[d].title, "state": data[d].state, "description": data[d].description, "id": data[d].id})
				datarec.children[datarec.children.length - 1] = addChildren(clone(datarec.children[datarec.children.length - 1]), data[d].id)
			}
		}
		return clone(datarec)
	}

	data2 = addChildren(clone(data2), data[0].id)
	return data2
}